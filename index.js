var _ = require('lodash');
const fs = require('fs');

const DATASET_LENGTH = 50;
const MAX_PATH_LENGTH = 30;
const CONST_DATASET = [
  { path_id: 0, path: [ 0, 1, 2, 3, 4 ] },
  { path_id: 1, path: [ 0, 3, 2, 3, 4 ] },
  { path_id: 2, path: [ 0, 1, 2, 3, 4 ] },
  { path_id: 3, path: [ 1, 0, 2, 3, 4 ] },
  { path_id: 4, path: [ 2, 1, 0, 3, 4 ] },
  { path_id: 5, path: [ 0, 1, 2, 3, 4 ] },
  { path_id: 6, path: [ 0, 2, 3, 4, 1 ] },
  { path_id: 7, path: [ 1, 0, 2, 3, 4 ] },
  { path_id: 8, path: [ 0, 1, 2, 3, 4 ] },
  { path_id: 9, path: [ 0, 2, 3, 1, 4 ] }
]

const zones = [
	{
		name: 'sala gialla',
		id: 0
	},
	{
		name: 'sala arancione',
		id: 1
	},
	{
		name: 'sala rossa',
		id: 2
	},
	{
		name: 'sala viola',
		id: 3
	},
	{
		name: 'sala blu',
		id: 4
	},
	{
		name: 'sala gialla',
		id: 5
	},
	{
		name: 'sala arancione',
		id: 6
	},
	{
		name: 'sala rossa',
		id: 7
	},
	{
		name: 'sala viola',
		id: 8
	},
	{
		name: 'sala blu',
		id: 9
	},
	{
		name: 'sala gialla',
		id: 10
	},
	{
		name: 'sala arancione',
		id: 11
	},
	{
		name: 'sala rossa',
		id: 12
	},
	{
		name: 'sala viola',
		id: 13
	},
	{
		name: 'sala blu',
		id: 14
	}
]

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function randomPath() {
	let rtn = [];
	let length = getRandomInt(3, MAX_PATH_LENGTH);
	for (i = 0; i < length; i++) {
		rtn.push(zones[getRandomInt(0, zones.length-1)].id);
	}
	return rtn
}

function randomPathUnique() {
	return _.shuffle(zones.map(el => el.id))	
}

function createDataset() {
	let rtn = [];
	for (let i = 0; i < DATASET_LENGTH; i++) {
		rtn.push({
			id: i,
			path: randomPath()
			// path: randomPathUnique()
		})
	}
	return rtn;
}

function convertDataset(dataset) {
	let rtn = {
		nodes: [],
		links: []
	};

	const nodesLength = Math.max.apply(null, dataset.map(el => el.path.length))
	let nodeId = 0;

	// loop the maximum number of steps
	for (var i = 0; i < nodesLength; i++) {
		dataset.forEach(visit => {
			let zone = visit.path[i];
	
			// COMPUTE NODES
			if (zone != undefined) {
				let node = rtn.nodes.find(el => el.step == i && el.zone == zone);
				if (!node) {
					rtn.nodes.push({
						id: nodeId,
						step: i,
						zone: zone,
						total: 1
					})
					nodeId++;
				} else {
					node.total++;
				}	
			}
		})
	}
	// loop the maximum number of steps
	for (var i = 0; i < nodesLength; i++) {
		dataset.forEach(visit => {
			let zone_from = visit.path[i];
			let zone_to = visit.path[i+1];

			// COMPUTE LINKS
			if (zone_to != undefined && zone_from != undefined) {
				let node_from = rtn.nodes.find(el => el.step == i && el.zone == zone_from)
				let node_to = rtn.nodes.find(el => el.step == i + 1 && el.zone == zone_to)

				let link = rtn.links.find(el => el.source == node_from.id && el.target == node_to.id);
				if (link) {
					link.value++;
				} else {
					rtn.links.push({
						source: node_from.id,
						target: node_to.id,
						value: 1
					})
				}				
			}
		})
	}

	return rtn;
}

function main() {
	// let dataset = createDataset();
	let dataset = CONST_DATASET;
	// console.log(dataset);
	let convertedDataset = convertDataset(dataset);
	let all = {...convertedDataset, paths: dataset, zones};
	console.log(all);

	let data = JSON.stringify(all);
	fs.writeFileSync('generic_not-random_ZONES=15_PATH-LENGTH=5.json', data);

}

main()